/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mystoreClient;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author DemTran
 * tạo 1 interface ở bên client để tiện cho quá trình đồng bộ hóa. ???? 
 * vẫn chưa hiểu tại sao wtf..... chẳng lẽ nó gửi dữ liệu cho nhau qua interface chú không phải là qua input
 * outputstream à???
 * 
 */
public interface ServerInterface extends Remote {
    
   /**
    * phương thức này trả lại true khi kết nối được tới server
    * @throws RemoteException the remote exception
    */
    public boolean connect(ClientInterface fileCI) throws RemoteException;
    
    /**
     * phương thức dùng để disconect
     * tại sao ở đây lại dùng void nhờ
     * đầu vào là 
     */
    public void disconnect(ClientInterface fileCI) throws RemoteException;
    
    
    /**
     * phương thức cho phép nhìn thấy trạng thái đồng bộ hóa
     * đầu vào là một client
     */
    public void showSyncState(ClientInterface fileCI) throws RemoteException;
    
    /**
     *bắt đầu kết nối
     */
    public void start() throws RemoteException;
    
    /**
     * ngắt kết nối
     */
    public void stop() throws RemoteException;
    
    /**
     * trả về true nếu trạng thái là start
     */
    public boolean isStart() throws RemoteException;
    
    /**
     *trả về server file
     */
    public File getFile() throws RemoteException;
    
    /**
     * thay đổi file phía server 
     */
    public File setFile(File serverFile) throws RemoteException;
    
    /**
     * dùng để gửi dữ liệu ra ngoài
     */
    public OutputStream getFileOutputStream(File f) throws RemoteException;
    
    /**
     * dùng để nhận dữ liệu
     */
    public InputStream getFileInputStream(File f) throws RemoteException;
    
    /**
     *hàm này dùng để lưu trả về danh sách các client đã kết nối
     */
    public ArrayList getConnected() throws RemoteException;
    
}

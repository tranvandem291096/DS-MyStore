/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mystoreserver;

import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author NgaPC
 */
public interface ClientInterface extends Remote {
    public InetAddress getAddress() throws RemoteException;
    public void setSyn(int state) throws RemoteException;
    public String getSyn() throws RemoteException;
}
